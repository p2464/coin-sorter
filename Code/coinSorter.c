#include <Servo.h>
#include <SevSeg.h>

#define trigPin 13
#define echoPin 12
#define button 2
#define led 8
#define led2 9

Servo motorPin;
SevSeg sevseg;

long distance, duration;
int coinType, coinTime, value, buttonState;

int debounceDelay, lastDebounceTime, buttonTime;

void setup() {
  //This section sets up the variables, serial output, ultrasound, and servo motor
  Serial.begin(9600);
  motorPin.attach(7);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(led, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(button, INPUT);
  coinTime = 0;
  value = 0;
  buttonState = 0;
  debounceDelay = 25;
  buttonTime = 0;
  motorPin.write(45);

  //next section sets up SevSeg display
  byte numDigits = 4;
  byte digitPins[] = {33, 32, 30, 31};
  byte segmentPins[] = {48, 46, 49, 47, 45, 44, 43, 41};

  bool resistorsOnSegments = true;
  bool updateWithDelaysIn = true;
  byte hardwareConfig = COMMON_CATHODE;
  sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments);
  sevseg.setBrightness(90);
}

void updateDisplay(int value) {          //updates SevSeg display given the inputted value
  sevseg.setNumber(value, 2);
  sevseg.refreshDisplay();
}

void resetValue() {                       //sets value back to zero (Used once change is taken out)
  value = 0;
}

/*void determineValue(String coin) {
  if (coin == Penny) {
    value += 0.01;
  }
  else if (coin == "Nickel") {
    value += 0.05;
  }
  else if (coin == "Dime") {
    value += 0.10;
  }
  else if (coin == "Quarter") {
    value += 0.25;
  }
  else if (coin == "Half-Dollar") {
    value += 0.5;
  }
  else {
    value += 1;
  }
  }
*/

void determineCoin(int coinTime) {          //determines coin through time taken to pass ultrasonic and updates value
  if (coinTime > 500) {                     //Initially called String coin and updated value through another function, but rather inefficient
    motorPin.write(0);
    //coin == "Dollar";
    value += 100;
  }
  else if ((coinTime <= 500) && (coinTime > 350)) {
    motorPin.write(12);
    //coin == "Half-Dollar";
    value += 50;
  }
  else if ((coinTime <= 350) && (coinTime > 250)) {
    motorPin.write(24);
    //coin == "Quarter";
    value += 25;
  }
  else if ((coinTime <= 250) && (coinTime > 150)) {
    motorPin.write(57);
    //coin == "Dime";
    value += 10;
  }
  else if ((coinTime <= 150) && (coinTime > 100)) {        //REMEMBER TO PUT EQUALS SIGN SOMEWHERE
    motorPin.write(69);
    //coin == "Nickel";
    value += 5;
  }
  else {
    motorPin.write(81);
    //coin == "Penny";
    value += 1;
  }
  updateDisplay(value);                        //Once value is determined, updateDisplay is called and updates accordingly
}

void loop() {                                  //loop constantly takes in the distance measurement from the ultrasonic and
  //based on the time, will determine what functions to call.

  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(2);
  digitalWrite(trigPin, LOW);

  buttonState = digitalRead(button);

  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) / 29.1;

  if (distance < 6) {                           //If distance less than 6, coinTime inreases value
    digitalWrite(led, HIGH);
    digitalWrite(led2, LOW);
    coinTime ++;
  }
  else {
    digitalWrite(led, LOW);
    digitalWrite(led2, HIGH);
    if (coinTime > 50) {                         //If cointime is greater than 50, then a coin has passed through
      Serial.println(coinTime);
      determineCoin(coinTime);
      coinTime = 0;
    }
  }

  if(buttonState == HIGH){
    buttonTime ++;
  }

  if((buttonTime - debounceDelay)>0){
    resetValue();
    updateDisplay(value);
  }

  /*if (distance >= 200 || distance <= 0) {
    Serial.println("Out of range");
    }
    else {
    Serial.print(distance);
    Serial.println(" cm");
    }
  */
  updateDisplay(value);
}